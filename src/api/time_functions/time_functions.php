<?php
use Symfony\Component\HttpFoundation\Request;
use Silex\Application;
date_default_timezone_set('America/Los_Angeles');

// Nicely formatted date
$timeOfDay = function (Request $request, Application $app){
	/*
	* D - short string representation of day of the week, e.g. Mon
	* M - short string representation of the month, e.g Jan
	* j - day of the month w/ no leading zeroes, i.e. 1 through 31
	* H - hour of day in 24hr format with leading zeroes, e.g. 5pm = 17th hour of the day
	* i - minutes past the hour with leading zeroes
	* s - seconds past the minute with leading zeroes
	* T - timezone abbreviation
	* Y - the year
	*/
	$today = date('D M j H:i:s T Y');
	$request->headers->set('date', $today);
	$date_string = '<strong>The date is: '.$today.'</strong>';
	return $date_string;
};

// Answer to the question, "What is today?"
$whatIsToday = function (Request $request, Application $app){
	$today = date('l F jS, Y'); // today's date
	$time = date('g:i A T'); // the current time
	$request->headers->set('date', $today);
	$request->headers->set('time', $time);
	$date_string = '<strong>Today is '.$today.'</strong>';
	$time_string = '<strong>The current time is '.$time.'</strong>';
	return $date_string.'<br>'.$time_string;
};

// RFC 2822 formated date
$todRFC = function (Request $request, Application $app){
	/*
	* r - RFC 2822 formatted date-time string
	*/
	$today = date('r');
	$request->headers->set('date', $today);
	return $today;
};

// ISO 8601 formatted date
$todISO = function (Request $request, Application $app){
	/*
	* c - ISO 8601 formatted date-time string
	*/ 
	$today = date('c');
	$request->headers->set('date', $today);
	return $today;
};

// Add the route matching functionality
// Nicely formatted time of day
$app->match('/date', $timeOfDay)->before($ssoProtect);

// Answer to the question, "What is today?"
$app->match('/date/today', $whatIsToday);

// RFC 2822 date
$app->match('/date/rfc', $todRFC);

// ISO 8601 date
$app->match('/date/iso', $todISO);
?>