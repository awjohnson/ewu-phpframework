<?php
namespace Forms\Test;

use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Mapping\ClassMetadata;

/**
 * @Entity @Table(name="test_users")
 */
class User
{
    /** @Id @Column(type="string") **/
    protected $netId;

    /** 
     * @OneToMany(targetEntity="Entry", mappedBy="user")
     * @var Entry[]
     **/
    protected $entries = null;

    public function __construct($netId)
    {
        // Do I need to lower this? SSO Should always provide it the same...
        $this->netId = $netId;
        $this->entries = new ArrayCollection();
    }

    public function getNetId()
    {
        return $this->netId;
    }

    public function getEntries()
    {
        return $this->entries;
    }
    public function addEntry($e)
    {
        $this->entries[] = $e;
    }
}


/**
 * @Entity @Table(name="test_entries") @HasLifecycleCallbacks
 **/
class Entry
{
    /** @Id @Column(type="integer") @GeneratedValue **/
    protected $id;

    /** @Column(name="agree", type="boolean", nullable=false) **/
    protected $agree;

    /** @Column(name="statement", type="text", nullable=false) **/
    protected $statement;

    /**
     * @ManyToOne(targetEntity="User", inversedBy="entries")
      * @JoinColumn(name="netId", referencedColumnName="netId")
    */
    protected $user;

    /**
     * @Column(name="created_at", type="datetime", nullable=false) 
     **/
    protected $createdAt;

    /**
     * @Column(name="updated_at", type="datetime", nullable=false) 
     **/
    protected $updatedAt;

    public function getId()
    {
        return $this->id;
    }

    public function getUser()
    {
        return $this->user;
    }
    public function setUser($user)
    {
        $user->addEntry($this);
        $this->user = $user;
    }

    public function getAgree()
    {
        return $this->agree;
    }
    public function setAgree($agree)
    {
        $this->agree = $agree;
    }

    public function getStatement()
    {
        return $this->statement;
    }
    public function setStatement($text)
    {
        $this->statement = $text;
    }

    /**
     * @PrePersist
     **/
    public function setCreatedAt()
    {
        $this->createdAt = new \DateTime();
        $this->updatedAt = new \DateTime();
    }

    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @PreUpdate
     **/
    public function setUpdatedAt()
    {
        $this->updatedAt = new \DateTime();
    }

    public function getUpdatedAt()
    {
        return $this->createdAt;
    }

    public static function loadValidatorMetadata(ClassMetadata $metadata)
    {
        $metadata->addPropertyConstraint('agree', new NotBlank(array(
            'message' => 'You must agree to the terms & conditions',
        )));
        $metadata->addPropertyConstraint('statement', new NotBlank(array(
            'message' => 'You must provide a statement',
        )));
    }
}
