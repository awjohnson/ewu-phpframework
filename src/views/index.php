<html>
<head>
<title>EWU | Project Builder Tools</title>
	<!--style>{{ source('homepagestylesheet.css') }}</style-->
	<link rel="stylesheet" href="css/foundation.css">
	<link rel="stylesheet" href="css/app.css">
</head>

<body>
	<form action="index.php" method="post">
	<script src="js/vendor/jquery.js"></script>
	<script src="js/foundation.min.js"></script>

	<!-- Top bar -->
	<div class="top-bar ewu-red centered">
		<img class="centered" src="images/ewu-logo.jpg" style="width: 65%; height: auto; border: 2px solid black;"></img>
	</div>
	<!-- End Top bar -->

	<div>
		<h1 class="centered">Project Builder Tools</h1>
	</div>

	<hr style="margin-left: 10%; margin-right: 10%;"></hr>

	<div class="centered" style="margin-left:10%; margin-right:10%;font-size: 18px;">
		<p>
			The following toolset is provided by the Information Technology department. 
			The available tools are meant to assist in the development of Senior Projects. 
			Developers who are extending this project, please see the bottom of the page for
			documentation.
		</p>
	</div>
	
	<div>
		<!-- Separator -->
		<hr style="margin-left: 10%; margin-right: 10%;"></hr>
	<!-- Tool Table -->
	<table class="ewu-red centered" style="width: 80%; height: auto; border: 2px, solid, black;">
		
		<h2 class="centered">Available tools:</h2>
		

		<tr></br></tr>

		<thead>
		<tr>
		  <th class="centered" width="auto">Name</th>
		  <th class="centered" width="auto">Description</th>
		  <th class="centered" width="auto">Link to Tool</th>
		  <th class="centered" width="auto">Documentation</th>
		  <th class="centered" width="auto">Example</th>

		</tr>
	  </thead>
		
		<!-- SSO -->
		<tr class="tablerow">
			<td><strong>Single Sign On</strong></td>
				<td> 
				Easily integrate EWU SSO into your project with attributes available.
				</br></br>		
				There is also a .NET version available.
				</td>
			<td>
				<a href="/date">PHP SSO Tool</a>
				</br></br>
				<a></a>
			</td>
			
			<td>
				<a href="https://bitbucket.org/easternwashingtonuniversityit/ewu-phpcas">README</a>
				</br></br>
				<a href="https://bitbucket.org/easternwashingtonuniversityit/ewu-dotnetcas">README</a>

			</td>	
			
			<td>
				<a href="https://bitbucket.org/easternwashingtonuniversityit/ewu-phpcas-example/overview">Source</a>
				</br></br>
				<a href="https://bitbucket.org/easternwashingtonuniversityit/ewu-dotnetcas-example/overview">Source</a>
			</td>
		</tr>
		
		
		<!-- OAuth - BitBucket, Google-->
		<tr class="tablerow">
			<td><strong>OAuth</strong></td>
			
			<td> 
				OAuth example using Bitbucket
				</br></br>
				OAuth example using Google
			</td>
			
			<td>
				<a href="/bit/">BitBucket Tool</a>
				</br></br>
				<a href="/google/">Google Tool</a>
			</td>
			
			<td>
				<a href="#">README</a>
				</br></br>
				<a href="#">README</a>
			</td>
			
			<td>
				<a href="#">Source</a>
				</br></br>
				<a href="#">Source</a>
			</td>
		</tr>

			
			
		<!-- Database Management -->
		<tr class="tablerow">
			<td><strong>Database Management</strong></td>
			<td> 
				Manage your database. 
			</td>
			
			<td>
				<a href="/db/">DB Management Tool</a>
			</td>
			
			<td>
				<a href="#">README</a>
			</td>
			
			<td>
				<a href="#">Source</a>
			</td>
		</tr>
	</table>
	<!-- End Tool Table -->
	</div>

	<!-- Separator -->
	<hr style="margin-left: 10%; margin-right: 10%;"></hr>

	<!-- Developers Section -->
	<div class="centered" style="margin-bottom: 50px">
		<h2>Developers:</h2>

		<table class="ewu-red centered" style="width: 35%; height:auto;">
			<tr class="tablerow" >
				<td>
					<a href="#">About this framework</a>
					</br></br>
					<a href="#">Creating a route</a>
					</br></br>
					<a href="#">About this homepage</a>
				</td>
			<tr>
		</table>
	</div>

	<div class="footer ewu-red"></div>

</body>
</html>
