# EWU Framework Homepage
The purpose of this documentation is to give you an idea of what the homepage represents and accomplishes. The homepage is used to represent the framework as a whole for both users and developers. 

## How it's made
The homepage is built simply by using HTML and CSS. To make it look nice, we used a front-end framework called [Foundation](http://foundation.zurb.com).

## Content
Currently, the content of the homepage includes each tool that is available within the framework. For each tool, there are links that correspond to the actual tool on the framework, it's associated README file, and the example source code. 

For the README files and source code, we have simply linked them to the URL within the BitBucket account.

## Structure
The tools are listed simply by using a table. Each tablerow represents a tool. Within each row, there are a number of tablecells. Here is an example of how to add a new tool to the table:
```
<!-- Comment name of tool here -->
<tr class="tablerow">
	<td><strong> Name of Tool </strong></td>
	<td> Description of Tool </td>
	<td><a href="Link/To/Tool">Name of Tool</a></td>
	<td><a href="Link/To/ReadMe">README</a></td>
	<td><a href="Link/To/Example/Source/Code">Source</a></td>
</tr>
```
After the tools table, there is a smaller table that provides documentation for future developers. For this table, all we have done is inserted a link to the corresponding README file (which is probably how you ended up here). In this table, we have used one tablerow for all of the documentation. So to add a new document to this, just follow this template:
```
</br></br>
<a href="Link/To/ReadMe">Name</a>
```
## Stylesheet
Since we are using Foundation for assistance with styling, to follow their standards, any changes you want to make will need to be done to the 'app.css' file located in the 'css' folder.

The 'app.css' file already has a fair amount of styling, much of which contains the values that follow the [EWU Identity Standards](https://m.access.ewu.edu/Documents/MarComm/EWU_IdentityStandards14.pdf).

Please get yourself familiar with the current css elements.

